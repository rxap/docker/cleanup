FROM alpine:latest

ENV CLEAN_PERIOD=**None** \
    DELAY_TIME=**None** \
    KEEP_IMAGES=**None** \
    KEEP_CONTAINERS=**None** \
    LOOP=true \
    DEBUG=0 \
    DOCKER_API_VERSION=1.40

# run.sh script uses some bash specific syntax
RUN apk add --update bash docker grep

# Install cleanup script
ADD src/docker-cleanup-volumes.sh /docker-cleanup-volumes.sh
ADD src/run.sh /run.sh

ENTRYPOINT ["/run.sh"]
